﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using InternetShop.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InternetShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly CategoriesService categoriesService;

        public CategoriesController(CategoriesService service)
        {
            categoriesService = service;
        }

        // GET api/values
        [HttpGet]
        public List<Category> Get()
        {
            return categoriesService.GetCategories();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Category Get(int id)
        {
            return categoriesService.GetCategory(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Category category)
        {
            categoriesService.Create(category);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromBody] Category category)
        {
            categoriesService.Update(category);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            categoriesService.Delete(id);
        }
    }
}
