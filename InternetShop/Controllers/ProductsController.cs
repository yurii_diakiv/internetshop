﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using InternetShop.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InternetShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductsService productsService;

        public ProductsController(ProductsService service)
        {
            productsService = service;
        }

        // GET api/values
        [HttpGet]
        public List<Product> Get()
        {
            return productsService.GetProducts();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Product Get(int id)
        {
            return productsService.GetProduct(id);
        }

        [Route("GetCountOfProductsByCategoryId/{id}")]
        [HttpGet]
        public int GetCountOfProductsByCategoryId(int id)
        {
            return productsService.GetCountOfProductsByCategoryId(id);
        }

        //api/person/byCategoryId?catId=1&from=a&to=b
        [HttpGet("byCategoryId")]
        public List<Product> GetProductsByCategoryId(int catId, int from, int to)
        {
            return productsService.GetProductsByCategoryId(catId, from, to);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Product product)
        {
            productsService.Create(product);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put([FromBody] Product product)
        {
            productsService.Update(product);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            productsService.Delete(id);
        }
    }
}
