﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetShop.Interfaces
{
    public interface ICategoriesService
    {
        List<Category> GetCategories();
        Category GetCategory(int id);
        void Create(Category item);
        void Update(Category item);
        void Delete(int id);
    }
}
