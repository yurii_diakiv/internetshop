﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetShop.Interfaces
{
    public interface IProductsService
    {
        List<Product> GetProducts();
        Product GetProduct(int id);
        void Create(Product item);
        void Update(Product item);
        void Delete(int id);
    }
}
