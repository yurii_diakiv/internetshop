﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DataAccess.Entities;
using InternetShop.Interfaces;
using DataAccess.Interfaces;
using DataAccess.Repositories;

namespace InternetShop.Services
{
    public class ProductsService : IProductsService
    {
        private readonly ProductRepository repository;

        public ProductsService(ProductRepository productRepository)
        {
            repository = productRepository;
        }

        public List<Product> GetProducts()
        {
            return repository.GetItems();
        }
        public Product GetProduct(int id)
        {
            return repository.GetItem(id);
        }

        public int GetCountOfProductsByCategoryId(int id)
        {
            return repository.GetCountOfItemsByCategoryId(id);
        }

        public List<Product> GetProductsByCategoryId(int catId, int from, int to)
        {
            return repository.GetItemsByCategoryId(catId, from, to);
        }

        public void Create(Product item)
        {
            repository.Create(item);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }

        public void Update(Product item)
        {
            repository.Update(item);
        }
    }
}
