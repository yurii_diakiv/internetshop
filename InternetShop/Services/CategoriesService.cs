﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DataAccess.Entities;
using InternetShop.Interfaces;
using DataAccess.Interfaces;
using DataAccess.Repositories;

namespace InternetShop.Services
{
    public class CategoriesService : ICategoriesService
    {
        private readonly IRepository<Category> repository;

        public CategoriesService(CategoryRepository categoryRepository)
        {
            repository = categoryRepository;
        }

        public List<Category> GetCategories()
        {
            return repository.GetItems();
        }
        public Category GetCategory(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(Category item)
        {
            repository.Create(item);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }

        public void Update(Category item)
        {
            repository.Update(item);
        }
    }
}
