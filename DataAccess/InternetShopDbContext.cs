﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class InternetShopDbContext : DbContext
    {
        public InternetShopDbContext(DbContextOptions<InternetShopDbContext> options)
            :base(options){}
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var products = new List<Product>
            {
                new Product {Id = 1, Name = "iPhone 6s 64gb Space Gray", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 2, Name = "iPad Pro 12.9 2018", CategoryId = 2, Description = "The most advanced iPad ever", Price = 750, Photo = "../../assets/images/iPadPro12.9_2018.jpg"},
                new Product {Id = 3, Name = "MacBook Pro Touch Bar 15 2019", CategoryId = 3, Description = "More power. More performance. More pro.", Price = 1200, Photo = "../../assets/images/MacBookPro15_2019.jpg"},
                new Product {Id = 4, Name = "iPhone 7 64gb Black", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
                new Product {Id = 5, Name = "Apple Watch Series 3", CategoryId = 5, Description = "42mm Space Gray Aluminum Case with Black Sport Band", Price = 300, Photo = "../../assets/images/appleWatchSeries3.jpg"},
                new Product {Id = 6, Name = "Apple AirPods 2019 with Charging Case", CategoryId = 4, Description = "Increased phone talk time. Activate Siri with voice. " +
                "New case with wireless charging capability. AirPods - Unique Wireless Headphones. They will fit all your devices. Take them out of the case and you can use it right away. " +
                "Just put them on, and they will instantly establish a connection, which means you can immediately immerse yourself in a rich quality sound. As if by magic.", Price = 249, Photo = "../../assets/images/Airpods.jpg"},
                new Product {Id = 7, Name = "iPhone 6s 64gb Space Gray 2", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 8, Name = "iPhone 6s 64gb Space Gray 4", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 9, Name = "iPhone 6s 64gb Space Gray 5", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 10, Name = "iPhone 6s 64gb Space Gray 8", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 11, Name = "iPhone 7 64gb Black 2", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
                new Product {Id = 12, Name = "iPhone 7 64gb Black 3", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
                new Product {Id = 13, Name = "iPhone 7 64gb Black 4", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
                new Product {Id = 14, Name = "iPhone 6s 64gb Space Gray 3", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 15, Name = "iPhone 7 64gb Black 5", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
                new Product {Id = 16, Name = "iPhone 7 64gb Black 6", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
                new Product {Id = 17, Name = "iPhone 6s 64gb Space Gray 6", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 18, Name = "iPhone 6s 64gb Space Gray 7", CategoryId = 1, Description = "The phone is made in an aluminum case, " +
                "with very good build quality and the most pleasant design.", Price = 500, Photo = "../../assets/images/iPhone6s.jpg"},
                new Product {Id = 19, Name = "iPhone 7 64gb Black 7", CategoryId = 1, Description = "Camera 12 MP. Retina HD display. " +
                "High performance and long work without recharging. Protection against water and dust. And stereo speakers.", Price = 600, Photo = "../../assets/images/iPhone7.jpg"},
            };

            var categories = new List<Category>
            {
                new Category {Id = 1, Name = "iPhone"},
                new Category {Id = 2, Name = "iPad"},
                new Category {Id = 3, Name = "MacBook"},
                new Category {Id = 4, Name = "Headphones"},
                new Category {Id = 5, Name = "Watch"}
            };

            modelBuilder.Entity<Product>().HasData(products);
            modelBuilder.Entity<Category>().HasData(categories);
            base.OnModelCreating(modelBuilder);
        }
    }
}
