﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly InternetShopDbContext dbContext;

        public ProductRepository(InternetShopDbContext shopDbContext)
        {
            dbContext = shopDbContext;
        }

        public List<Product> GetItems()
        {
            return dbContext.Set<Product>().ToList();
        }

        public Product GetItem(int id)
        {
            return dbContext.Set<Product>().Find(id);
        }

        public int GetCountOfItemsByCategoryId(int id)
        {
            return dbContext.Set<Product>().Where(p => p.CategoryId == id).Count();
        }

        public List<Product> GetItemsByCategoryId(int catId, int from, int to)
        {
            List<Product> products = dbContext.Set<Product>().Where(p => p.CategoryId == catId).ToList();
            List<Product> products1 = new List<Product>();
            if(to > products.Count)
            {
                return products;
            }
            for(int i = from; i < to; ++i)
            {
                products1.Add(products[i]);
            }
            return products1;
        }
        public void Create(Product item)
        {
            dbContext.Set<Product>().Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<Product>().Find(id);
            dbContext.Set<Product>().Remove(p);
            dbContext.SaveChanges();
        }

        public void Update(Product item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
