﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly InternetShopDbContext dbContext;

        public CategoryRepository(InternetShopDbContext shopDbContext)
        {
            dbContext = shopDbContext;
        }

        public List<Category> GetItems()
        {
            return dbContext.Set<Category>().ToList();
        }

        public Category GetItem(int id)
        {
            return dbContext.Set<Category>().Find(id);
        }
        public void Create(Category item)
        {
            dbContext.Set<Category>().Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<Category>().Find(id);
            dbContext.Set<Category>().Remove(p);
            dbContext.SaveChanges();
        }

        public void Update(Category item)
        {
            dbContext.Entry(item).State = EntityState.Modified;
            dbContext.SaveChanges();
        }
    }
}
