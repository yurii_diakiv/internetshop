﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddNewSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Description",
                value: "The phone is made in an aluminum case, with very good build quality and the most pleasant design.");

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Description", "Name", "Photo", "Price" },
                values: new object[,]
                {
                    { 7, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 2", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 8, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 4", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 9, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 5", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 10, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 8", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 11, 1, "Camera 12 MP. Retina HD display. High performance and long work without recharging. Protection against water and dust. And stereo speakers.", "iPhone 7 64gb Black 2", "../../assets/images/iPhone7.jpg", 600.0 },
                    { 12, 1, "Camera 12 MP. Retina HD display. High performance and long work without recharging. Protection against water and dust. And stereo speakers.", "iPhone 7 64gb Black 3", "../../assets/images/iPhone7.jpg", 600.0 },
                    { 13, 1, "Camera 12 MP. Retina HD display. High performance and long work without recharging. Protection against water and dust. And stereo speakers.", "iPhone 7 64gb Black 4", "../../assets/images/iPhone7.jpg", 600.0 },
                    { 14, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 3", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 15, 1, "Camera 12 MP. Retina HD display. High performance and long work without recharging. Protection against water and dust. And stereo speakers.", "iPhone 7 64gb Black 5", "../../assets/images/iPhone7.jpg", 600.0 },
                    { 16, 1, "Camera 12 MP. Retina HD display. High performance and long work without recharging. Protection against water and dust. And stereo speakers.", "iPhone 7 64gb Black 6", "../../assets/images/iPhone7.jpg", 600.0 },
                    { 17, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 6", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 18, 1, "The phone is made in an aluminum case, with very good build quality and the most pleasant design.", "iPhone 6s 64gb Space Gray 7", "../../assets/images/iPhone6s.jpg", 500.0 },
                    { 19, 1, "Camera 12 MP. Retina HD display. High performance and long work without recharging. Protection against water and dust. And stereo speakers.", "iPhone 7 64gb Black 7", "../../assets/images/iPhone7.jpg", 600.0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "Description",
                value: "Modern mobile phone");
        }
    }
}
